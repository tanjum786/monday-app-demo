import React, { useState } from "react";
import Search from "./components/Search";
import profileImage from "./Assets/images-removebg-preview.png";
import { useDataContext } from "./ContextAPI.js/GetDataProvider";

function BoardSideBar() {
  const {
    data,
    getChatData,
    setContactName,
    loader,
    setChatLoader,
    chatId,
    setChatId,
    setContactProfile,
  } = useDataContext();
  const [searchQuery, setSearchQuery] = useState("");

  const chatOpen = (id, name, phone, profile) => {
    getChatData(id);
    setChatId(id);
    setChatLoader(true);
    setContactProfile(profile);
    name ? setContactName(name) : setContactName(phone);
  };

  const filteredData = data?.filter((contact) => {
    const name = contact.name ? contact.name.toLowerCase() : "";
    const phone = contact.phone ? contact.phone.toLowerCase() : "";
    const query = searchQuery.toLowerCase();
    return name.includes(query) || phone.includes(query);
  });

  const TimeCoverter = (timestamp) => {
    const date = new Date(timestamp * 1000);
    const year = date.getFullYear();
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const day = ("0" + date.getDate()).slice(-2);
    const hours = ("0" + date.getHours()).slice(-2);
    const minutes = ("0" + date.getMinutes()).slice(-2);
    return `${year}-${month}-${day} ${hours}:${minutes}`;
  };
  return (
    <div className="board-Side-bar_container">
      <Search setSearchQuery={setSearchQuery} />
      <div className={`board-sidebar_wrapper ${loader ? "chat-loader" : ""}`}>
        {loader ? (
          <div class="circle-container">
            <div class="circle-progress"></div>
          </div>
        ) : (
          <>
            {filteredData?.map((contact) => (
              <div className="board_contacts" key={contact.id}>
                <button
                  onClick={() =>
                    chatOpen(
                      contact.id,
                      contact.name,
                      contact.phone,
                      contact.profile ? contact.profile : profileImage
                    )
                  }
                  className={`${contact.id === chatId ? "activeBtn" : ""}`}
                >
                  <div className="board_contacts-wrapper">
                    <div className="contact_profile">
                      <img
                        src={contact.profile ? contact.profile : profileImage}
                        alt=""
                      />
                    </div>
                    <div className="contact_name">
                      {contact.name ? (
                        <h3>{contact.name}</h3>
                      ) : (
                        <h3>{contact.phone}</h3>
                      )}
                      {/* <span>Chatgram Web was updated.</span> */}
                    </div>
                  </div>
                  <div className="new-msg_container">
                    <span>{TimeCoverter(contact.time)}</span>
                    {/* <span className="new-msg">1</span> */}
                  </div>
                </button>
              </div>
            ))}
          </>
        )}
      </div>
    </div>
  );
}

export default BoardSideBar;
