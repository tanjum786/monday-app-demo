import React, { useState } from "react";
import profile from "../Assets/main.jpg";
import searchIcon from "../Assets/Vector.png";
import "./search.css";

function Search({ setSearchQuery }) {
  const [searchTerm, setSearchTerm] = useState("");

  const handleInputChange = (event) => {
    const value = event.target.value;
    setSearchTerm(value);
    setSearchQuery(value);
  };
 
  return (
    <div className="search-container">
      <div className="search_profile-container">
        <div className="profile-img_container">
          <img src={profile} className="profile_img" alt="" />
        </div>
        <div className="search-container-wrapper">
          <img src={searchIcon} alt="" />
          <input
            type="text"
            placeholder="search"
            value={searchTerm}
            onChange={handleInputChange}
          />
        </div>
      </div>
    </div>
  );
}

export default Search;
