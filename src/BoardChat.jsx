import React, { useEffect, useRef, useState } from "react";
import searchIcon from "./Assets/Vector.png";
import clock from "./Assets/clock .svg";
// import dpubleClick from "./Assets/dobule white tick.svg";
import attachment from "./Assets/attachment.svg";

import profileImage from "./Assets/images-removebg-preview.png";
import { useDataContext } from "./ContextAPI.js/GetDataProvider";
import send from "./Assets/Vector (1).png";
import Picker from "emoji-picker-react";

function BoardChat() {
  const {
    contactName,
    chatData,
    chatLoader,
    sendMessage,
    chatId,
    sendLoader,
    contactProfile,
  } = useDataContext();
  const chatAreaRef = useRef(null);
  const [chatvalue, setChatValue] = useState("");
  const [showPicker, setShowPicker] = useState(false);

  let sendReplyInput = (e) => {
    setChatValue(e?.target?.value);
  };

  let handelReply = async () => {
    if (chatvalue) {
      const payload = {
        instance_id: 3180,
        user_id: "cmlzaGFiaEBuaXN3ZXkuY29tfDI3MjAzMjc=",
        message: chatvalue,
        phone: chatId,
      };
      sendMessage(payload);
      setChatValue("");
    }
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter" && chatvalue) {
      e.preventDefault();
      handelReply();
    }
  };

  const TimeConverter = (timestamp) => {
    const currentDate = new Date();
    const messageDate = new Date(timestamp * 1000);

    // Format time
    const hours = ("0" + messageDate.getHours()).slice(-2);
    const minutes = ("0" + messageDate.getMinutes()).slice(-2);

    // Check if the message was sent today
    if (
      currentDate.getDate() === messageDate.getDate() &&
      currentDate.getMonth() === messageDate.getMonth() &&
      currentDate.getFullYear() === messageDate.getFullYear()
    ) {
      return {
        date: "Today",
        time: `${hours}:${minutes}`,
      };
    } else {
      // Format date
      const year = messageDate.getFullYear();
      const month = ("0" + (messageDate.getMonth() + 1)).slice(-2);
      const day = ("0" + messageDate.getDate()).slice(-2);
      return {
        date: `${day}-${month}-${year}`,
        time: `${hours}:${minutes}`,
      };
    }
  };

  useEffect(() => {
    if (chatAreaRef.current) {
      chatAreaRef.current.scrollTo({
        top: chatAreaRef.current.scrollHeight,
        behavior: "smooth",
      });
    }
  }, [chatData]);

  function renderTextWithLinks(text) {
    const linkRegex = /(?:^|\s)((?:https?:\/\/)?(?:www\.)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
    return text.replace(linkRegex, (match, url) => {
      if (!url.startsWith("http")) {
        url = "http://" + url; // Prefix with http:// if no prefix present
      }
      return `<a href="${url}" target="_blank">${match}</a>`;
    });
  }

  return (
    <div
      className={`chat-container ${contactName ? "bg" : ""}`}
      ref={chatAreaRef}
    >
      {contactName ? (
        <div className="chat-wrapper">
          <div className="chat-heading">
            <div className="chat-heading_name-container">
              <div className="chat-profile_image">
                <img src={contactProfile} alt="" />
              </div>
              <div className="chat_profile-name">
                <h3>{contactName}</h3>
                {/* <span className="active">last seen 10.30pm</span> */}
              </div>
            </div>
            {/* <div className="search_container">
              <img src={searchIcon} alt="" />
            </div> */}
          </div>
          <div
            className={`chat-area ${chatLoader ? "chat-loader" : ""}`}
            ref={chatAreaRef}
          >
            {chatLoader ? (
              <div class="circle-container">
                <div class="circle-progress"></div>
              </div>
            ) : (
              <div className="msgs_area">
                {chatData?.map((personal) => (
                  <div
                    className={
                      personal?.fromMe === 0
                        ? "receiver-wrapper"
                        : "sender_wrapper"
                    }
                  >
                    <div
                      className={
                        personal?.fromMe === 0
                          ? "receiver-container"
                          : "sender_container"
                      }
                    >
                      {!personal?.files ? (
                        <p
                          className="text-color"
                          dangerouslySetInnerHTML={{
                            __html: renderTextWithLinks(personal.body),
                          }}
                        />
                      ) : (
                        personal?.files?.map((file, index) => {
                          if (file.extension === "mp4") {
                            return (
                              <div>
                                <video
                                  key={index}
                                  src={file.url}
                                  controls
                                ></video>
                              </div>
                            );
                          } else {
                            
                            return (
                              <div>
                                <img key={index} src={file.url} alt="" />
                              </div>
                            );
                          }
                        })
                      )}
                      {personal.caption && (
                        <div>
                          <p
                            className="text-color"
                            dangerouslySetInnerHTML={{
                              __html: renderTextWithLinks(personal.caption),
                            }}
                          />
                        </div>
                      )}
                      {/* {personal.body} */}
                      <div className="time-wrapper">
                        <div className="time-container">
                          <span className="message-time">
                            {TimeConverter(personal.time).time}
                          </span>
                          <span className="message-date">
                            {TimeConverter(personal.time).date}
                          </span>
                        </div>
                        {/* {personal?.fromMe === 1 ? (
                          <div className="waiting-image">
                            <img src={clock} alt="" srcset="" />
                            ) : (
                            <img src={dpubleClick} alt="" srcset="" />
                          </div>
                        ) : null} */}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
          <div className="message-wrapper">
            <div className="message-container">
              <div className="emoji-picker">
                <img
                  className="emoji-icon"
                  alt=""
                  src="https://icons.getbootstrap.com/assets/icons/emoji-smile.svg"
                  onClick={() => setShowPicker((val) => !val)}
                />
                {showPicker && (
                  <Picker
                    onEmojiClick={(emojiObject) => {
                      setChatValue((prevMsg) => prevMsg + emojiObject.emoji);
                      setShowPicker(false);
                    }}
                  />
                )}
              </div>
              <div>
                <button className="attachment-btn">
                  <input type="file" name="" id="" className="filesUpload" />
                  <img src={attachment} alt="" />
                </button>
              </div>
              <div>
                <input
                  type="text"
                  placeholder="Message"
                  className="send-input"
                  value={chatvalue}
                  onChange={sendReplyInput}
                  onKeyDown={handleKeyDown}
                  disabled={sendLoader}
                />
              </div>

              <div className="send-btn_container">
                {sendLoader ? (
                  <div class="loading"></div>
                ) : (
                  <button className="send-btn" onClick={handelReply}>
                    <img src={send} alt="" />
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}

export default BoardChat;
