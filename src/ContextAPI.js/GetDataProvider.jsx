import axios from "axios";
import React, { createContext, useContext, useState, useEffect } from "react";

const useData = createContext();

function GetDataProvider({ children }) {
  const [data, setData] = useState([]);
  const [chatData, setChatData] = useState([]);
  const [contactName, setContactName] = useState();
  const [loader, setLoader] = useState(true);
  const [chatLoader, setChatLoader] = useState(true);
  const [sendLoader, setSendLoader] = useState(false);

  const [responseData, setResponseData] = useState(null);
  const [contactProfile, setContactProfile] = useState();
  const [userProfile, setUserProfile] = useState();

  const [chatId, setChatId] = useState("");
  const [lastMessage, setLastMessage] = useState("");

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      const { data } = await axios.get(
        "https://niswey.net/v1/dialogs?user_id=cmlzaGFiaEBuaXN3ZXkuY29tfDI3MjAzMjc="
      );
      setData(data.data);

      setLoader(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoader(true);
    }
  };

  const getChatData = async (id) => {
    try {
      const response = await axios.get(
        `https://niswey.net/v2/chat?user_id=cmlzaGFiaEBuaXN3ZXkuY29tfDI3MjAzMjc=&chatId=${id}`
      );
      setChatData(response.data.messages);
      setChatLoader(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setChatLoader(true);
    }
  };
  const sendMessage = async (payload) => {
    try {
      setSendLoader(true);
      const newMessage = {
        id: Math.random()
          .toString(36)
          .substring(7),
        fromMe: 1,
        body: payload.message,
        time: Math.floor(Date.now() / 1000),
      };

      const response = await axios.post(
        "https://niswey.net/api/reply",
        payload
      );
      if (response.data.status === "ok") {
        setChatData((prevChatData) => [...prevChatData, newMessage]);
        setSendLoader(false);
      }

      setResponseData(response.data);
    } catch (error) {}
  };
  return (
    <useData.Provider
      value={{
        data,
        setData,
        getChatData,
        chatData,
        contactName,
        setContactName,
        loader,
        chatLoader,
        sendMessage,
        setChatLoader,
        chatId,
        setChatId,
        contactProfile,
        userProfile,
        setUserProfile,
        setContactProfile,
        sendLoader,
      }}
    >
      {children}
    </useData.Provider>
  );
}

const useDataContext = () => useContext(useData);

export { useDataContext, GetDataProvider };
