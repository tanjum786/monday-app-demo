import "./board.css";
import BoardSideBar from "./BoardSideBar";
import BoardChat from "./BoardChat";
let BoardView = () => {
  return (
    <>
      <div className="board-view-container container">
        <BoardSideBar />
        <BoardChat />
      </div>
    </>
  );
};
export default BoardView;
